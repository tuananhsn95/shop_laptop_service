package tungphongdo.com.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import tungphongdo.com.entity.ProductDetail;

@Repository
public interface ProductDetailRepository extends JpaRepository<ProductDetail, Integer> {

    @Query(value = "select \n" +
            "   pd.ID id,\n" +
            "   pd.PRODUCT_ID productId,\n" +
            "   p.PRODUCT_NAME productName,\n" +
            "   cg.ID categoryId,\n" +
            "   cg.CATEGORY_NAME categoryName,\n" +
            "   sz.ID sizeId,\n" +
            "   sz.SCREEN_SIZE screenName,\n" +
            "   cl.ID colorId,\n" +
            "   cl.COLOR_NAME colorName,\n" +
            "   pd.AMOUNT amount,\n" +
            "   pd.SINGLE_PRICE singlePrice,\n" +
            "   convert( pd.DATETIME_IMPORTED, char) datetimeImported,\n" +
            "   pd.DESCRIPTION description,\n" +
            "   pd.IMAGE image\n" +
            "from PRODUCT_DETAIL pd\n" +
            "   join PRODUCT p on pd.PRODUCT_ID = p.ID \n" +
            "   join CATEGORY cg on p.CATEGORY_ID = cg.ID\n" +
            "   join SIZE sz on pd.SIZE_ID = sz.ID\n" +
            "   join COLOR cl on pd.COLOR_ID = cl.ID\n" +
            "where 1=1\n" +
            "and (:categoryId is null or cg.ID = :categoryId) \n" +
            "and (:productId is null or pd.PRODUCT_ID = :productId) \n" +
            "and (:screenSize is null or sz.SCREEN_SIZE = :screenSize) \n" +
            "and (:colorName is null or lower(cl.COLOR_NAME) like lower(concat('%', :colorName, '%')) )",
            countQuery = "select count(1) \n" +
                    "from PRODUCT_DETAIL pd\n" +
                    "   join PRODUCT p on pd.PRODUCT_ID = p.ID \n" +
                    "   join CATEGORY cg on p.CATEGORY_ID = cg.ID\n" +
                    "   join SIZE sz on pd.SIZE_ID = sz.ID\n" +
                    "   join COLOR cl on pd.COLOR_ID = cl.ID\n" +
                    "where 1=1\n" +
                    "and (:categoryId is null or cg.ID = :categoryId) \n" +
                    "and (:productId is null or pd.PRODUCT_ID = :productId) \n" +
                    "and (:screenSize is null or sz.SCREEN_SIZE = :screenSize) \n" +
                    "and (:colorName is null or lower(cl.COLOR_NAME) like lower(concat('%', :colorName, '%')) )",
            nativeQuery = true)
    Page<Object[]> getAllProduct(@Param("categoryId") Integer categoryId,
                                 @Param("productId") Integer productId,
                                 @Param("screenSize") Long screenSize,
                                 @Param("colorName") String colorName,
                                 Pageable pageable);
}
