package tungphongdo.com.resourceRequest;

public class GetAllProductRequest {
    private Integer categoryId;
    private Integer productId;
    private Long screenSize;
    private String colorName;

    public GetAllProductRequest(Integer categoryId, Integer productId, Long screenSize, String colorName) {
        this.categoryId = categoryId;
        this.productId = productId;
        this.screenSize = screenSize;
        this.colorName = colorName;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Long getScreenSize() {
        return screenSize;
    }

    public void setScreenSize(Long screenSize) {
        this.screenSize = screenSize;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }
}
