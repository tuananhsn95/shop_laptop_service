package tungphongdo.com.resourceRequest;

import javax.validation.constraints.NotEmpty;

public class ChangePassRequest {
    private Integer id;

    @NotEmpty(message = "{current.password.is.not.empty}")
    private String currentPass;

    @NotEmpty(message = "{new.password.is.not.empty}")
    private String newPass;

    public ChangePassRequest(Integer id, String currentPass, String newPass) {
        this.id = id;
        this.currentPass = currentPass;
        this.newPass = newPass;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCurrentPass() {
        return currentPass;
    }

    public void setCurrentPass(String currentPass) {
        this.currentPass = currentPass;
    }

    public String getNewPass() {
        return newPass;
    }

    public void setNewPass(String newPass) {
        this.newPass = newPass;
    }
}
