package tungphongdo.com.exception;

public class ValidationException extends RuntimeException{
    private static final Long serialVersionUID = 1L;
    public  ValidationException(String message){
        super(message);
    }
}
