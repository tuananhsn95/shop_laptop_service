package tungphongdo.com.exception;

public class SendMailException extends RuntimeException {
    private static final Long serialVersionUID = 1L;

    public SendMailException(String message) {
        super(message);
    }
}
