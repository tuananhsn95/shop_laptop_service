package tungphongdo.com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ShopLaptopServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(ShopLaptopServiceApplication.class, args);
    }

}
