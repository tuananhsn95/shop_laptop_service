package tungphongdo.com.config;

import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.stereotype.Component;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import java.util.Locale;

@Component
public class MultiLanguageConfig {

    @Bean
    public LocaleResolver getLocaleResolver()  {
        SessionLocaleResolver slr = new SessionLocaleResolver();
        slr.setDefaultLocale(Locale.US);
        return slr;
    }

    @Bean
    public ResourceBundleMessageSource messageSource(){
        ResourceBundleMessageSource r   = new ResourceBundleMessageSource();
        r.setBasename("i18n/message");
        r.setUseCodeAsDefaultMessage(true);
        return r;
    }



//    //xác định ngôn ngữ hiển thị trên view bằng cách nào, ví dụ thông qua cookies
//    @Bean(name = "localeResolver")
//    public LocaleResolver getLocaleResolver()  {
//        CookieLocaleResolver resolver= new CookieLocaleResolver();
//        resolver.setCookieDomain("myAppLocaleCookie");
//        resolver.setDefaultLocale(Locale.ENGLISH);
//        // 60 minutes
//        resolver.setCookieMaxAge(60*60);
//        return resolver;
//    }
//
//    //khai báo các file properties
//    @Bean(name = "messageSource")
//    public MessageSource getMessageResource()  {
//        ReloadableResourceBundleMessageSource messageResource= new ReloadableResourceBundleMessageSource();
//
//        // Đọc vào file i18n/messages_xxx.properties
//        // Ví dụ: i18n/messages_en.properties
//        messageResource.setBasename("classpath:i18n/messages");
//        messageResource.setDefaultEncoding("UTF-8");
//        return messageResource;
//    }
//
//    //xác định thay đổi ngôn ngữ hiển thị thông qua param name nào trên trình duyệt
//    @Override
//    public void addInterceptors(InterceptorRegistry registry) {
//        LocaleChangeInterceptor localeInterceptor = new LocaleChangeInterceptor();
//        localeInterceptor.setParamName("language");
//        registry.addInterceptor(localeInterceptor).addPathPatterns("/*");
//    }



}
