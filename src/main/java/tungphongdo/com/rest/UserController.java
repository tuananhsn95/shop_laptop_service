package tungphongdo.com.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import tungphongdo.com.dto.UserDTO;
import tungphongdo.com.exception.ResourceNotFoundException;
import tungphongdo.com.resourceRequest.ChangePassRequest;
import tungphongdo.com.resourceRequest.LoginRequest;
import tungphongdo.com.resourceResponse.CommonResponse;
import tungphongdo.com.resourceResponse.GetDetailResponse;
import tungphongdo.com.resourceResponse.LoginResponse;
import tungphongdo.com.service.serviceImpl.UserServiceImpl;

import javax.validation.Valid;

@RestController
@RequestMapping(value = {"/api/user"})
@Validated
public class UserController {

    @Autowired
    private UserServiceImpl userServiceImpl;

    @PostMapping("/login")
    public ResponseEntity<LoginResponse> login(@RequestBody @Valid LoginRequest loginRequest) throws ResourceNotFoundException {
        return ResponseEntity.ok(userServiceImpl.login(loginRequest));
    }

    @PostMapping("/create-user")
    public ResponseEntity<CommonResponse> createUser(@RequestBody @Valid UserDTO userDTO) {
        return ResponseEntity.ok(userServiceImpl.createUser(userDTO));
    }

    @GetMapping("/forgot-pass")
    public ResponseEntity<CommonResponse> forgotPass(@RequestParam("email") String email) throws ResourceNotFoundException {
        return ResponseEntity.ok(userServiceImpl.forgotPass(email));
    }

    @PatchMapping("/change-pass")
    public ResponseEntity<CommonResponse> changePass(@RequestBody @Valid ChangePassRequest changePassRequest) throws Exception {
        return ResponseEntity.ok(userServiceImpl.changePass(changePassRequest));
    }

    @GetMapping("/detail")
    public ResponseEntity<GetDetailResponse> userDetail(@RequestParam("id") Integer id) throws Exception {
        return ResponseEntity.ok(userServiceImpl.userDetail(id));
    }
}
