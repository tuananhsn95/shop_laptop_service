package tungphongdo.com.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import tungphongdo.com.dto.ProductDTO;
import tungphongdo.com.dto.ProductDetailDTO;
import tungphongdo.com.resourceRequest.GetAllProductRequest;
import tungphongdo.com.resourceRequest.SearchRequest;
import tungphongdo.com.resourceResponse.ServiceResult;
import tungphongdo.com.service.serviceImpl.ProductDetailServiceImpl;

@RestController
@RequestMapping(value = {"/api/product"})
@Validated
public class ProductController {

    @Autowired
    private ProductDetailServiceImpl productDetailServiceImpl;


    @PostMapping("/get-all-product")
    public ResponseEntity<ServiceResult<ProductDetailDTO>> getAllProduct(@RequestBody SearchRequest<GetAllProductRequest> productSearchRequest) {
        return ResponseEntity.ok(productDetailServiceImpl.getAllProduct(productSearchRequest));
    }

    //for trying to get data using procedure
    @GetMapping("/get-product-by-id")
    public ResponseEntity<ProductDTO> getProductById(@RequestParam Integer productId){
        return ResponseEntity.ok(productDetailServiceImpl.getProductById(productId));
    }
}
