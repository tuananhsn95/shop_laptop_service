package tungphongdo.com.resourceResponse;

import java.util.Date;

public class GetDetailResponse {
    private Date timestamp;
    private Integer statusCode;
    private String message;
    private Object data;

    public GetDetailResponse(Date timestamp, Integer statusCode, String message, Object data) {
        this.timestamp = timestamp;
        this.statusCode = statusCode;
        this.message = message;
        this.data = data;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
