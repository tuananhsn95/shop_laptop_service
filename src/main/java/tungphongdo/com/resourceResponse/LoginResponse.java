package tungphongdo.com.resourceResponse;

import java.util.Date;

public class LoginResponse {
    private Date timestamp;
    private Integer statusCode;
    private String message;
    private String token;
    private String fullName;
    private Integer userId;

    public LoginResponse() {
    }

    public LoginResponse(Date timestamp, Integer statusCode, String message, String token, String fullName, Integer userId) {
        this.timestamp = timestamp;
        this.statusCode = statusCode;
        this.message = message;
        this.token = token;
        this.fullName = fullName;
        this.userId = userId;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }
}
