package tungphongdo.com.entity;

import tungphongdo.com.dto.SaleOffDTO;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "SALE_OFF")
public class SaleOff {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "SALE_OF_NAME")
    private String saleOffName;

    @Column(name = "START_TIME")
    private Timestamp startTime;

    @Column(name = "END_TIME")
    private Timestamp endTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSaleOffName() {
        return saleOffName;
    }

    public void setSaleOffName(String saleOffName) {
        this.saleOffName = saleOffName;
    }

    public Timestamp getStartTime() {
        return startTime;
    }

    public void setStartTime(Timestamp startTime) {
        this.startTime = startTime;
    }

    public Timestamp getEndTime() {
        return endTime;
    }

    public void setEndTime(Timestamp endTime) {
        this.endTime = endTime;
    }

    public SaleOffDTO toModel(){
        SaleOffDTO saleOffDTO = new SaleOffDTO();
        saleOffDTO.setId(this.id);
        saleOffDTO.setSaleOffName(this.getSaleOffName());
        saleOffDTO.setStartTime(this.startTime);
        saleOffDTO.setEndTime(this.getEndTime());
        return saleOffDTO;
    }
}
