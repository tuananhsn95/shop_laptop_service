package tungphongdo.com.entity;

import tungphongdo.com.dto.ProductDetailBillDTO;
import javax.persistence.*;

@Entity
@Table(name = "PRODUCT_DETAIL_BILL")
public class ProductDetailBill {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "PRODUCT_DETAIL_ID")
    private Integer productDetailId;

    @Column(name = "BILL_ID")
    private Integer billId;

    @Column(name = "AMOUNT")
    private Integer amount;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getProductDetailId() {
        return productDetailId;
    }

    public void setProductDetailId(Integer productDetailId) {
        this.productDetailId = productDetailId;
    }

    public Integer getBillId() {
        return billId;
    }

    public void setBillId(Integer billId) {
        this.billId = billId;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public ProductDetailBillDTO toModel(){
        ProductDetailBillDTO productDetailBillDTO = new ProductDetailBillDTO();
        productDetailBillDTO.setId(this.getId());
        productDetailBillDTO.setProductDetailId(this.getProductDetailId());
        productDetailBillDTO.setAmount(this.getAmount());
        productDetailBillDTO.setBillId(this.getBillId());
        return productDetailBillDTO;
    }
}
