package tungphongdo.com.entity;

import tungphongdo.com.dto.ProductDetailDTO;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "PRODUCT_DETAIL")
public class ProductDetail {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "PRODUCT_ID")
    private Integer productId;

    @Column(name = "SIZE_ID")
    private Integer sizeId;

    @Column(name = "COLOR_ID")
    private Integer colorId;

    @Column(name = "AMOUNT")
    private Integer amount;

    @Column(name = "SINGLE_PRICE")
    private Double singlePrice;

    @Column(name = "DATETIME_IMPORTED")
    private LocalDateTime datetimeImported;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "IMAGE")
    private String image;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Integer getSizeId() {
        return sizeId;
    }

    public void setSizeId(Integer sizeId) {
        this.sizeId = sizeId;
    }

    public Integer getColorId() {
        return colorId;
    }

    public void setColorId(Integer colorId) {
        this.colorId = colorId;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Double getSinglePrice() {
        return singlePrice;
    }

    public void setSinglePrice(Double singlePrice) {
        this.singlePrice = singlePrice;
    }

    public LocalDateTime getDatetimeImported() {
        return datetimeImported;
    }

    public void setDatetimeImported(LocalDateTime datetimeImported) {
        this.datetimeImported = datetimeImported;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public ProductDetailDTO toModel() {
        ProductDetailDTO productDetailDTO = new ProductDetailDTO();
        productDetailDTO.setId(this.getId());
        productDetailDTO.setProductId(this.getProductId());
        productDetailDTO.setSizeId(this.getSizeId());
        productDetailDTO.setColorId(this.getColorId());
        productDetailDTO.setAmount(this.getAmount());
        productDetailDTO.setSinglePrice(this.getSinglePrice());
        productDetailDTO.setDatetimeImported(this.getDatetimeImported());
        productDetailDTO.setDescription(this.getDescription());
        productDetailDTO.setImage(this.getImage());
        return productDetailDTO;
    }
}
