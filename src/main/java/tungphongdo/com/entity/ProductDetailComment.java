package tungphongdo.com.entity;

import tungphongdo.com.dto.ProductDetailCommentDTO;

import javax.persistence.*;

@Entity
@Table(name = "PRODUCT_DETAIL_COMMENT")
public class ProductDetailComment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "USER_ID")
    private Integer userId;

    @Column(name = "RATE")
    private Integer rate;

    @Column(name = "COMMENT")
    private String comment;

    @Column(name = "PRODUCT_DETAIL_ID")
    private Integer productDetailId;



    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getRate() {
        return rate;
    }

    public void setRate(Integer rate) {
        this.rate = rate;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Integer getProductDetailId() {
        return productDetailId;
    }

    public void setProductDetailId(Integer productDetailId) {
        this.productDetailId = productDetailId;
    }

    public ProductDetailCommentDTO toModel(){
        ProductDetailCommentDTO productDetailCommentDTO = new ProductDetailCommentDTO();
        productDetailCommentDTO.setId(this.getId());
        productDetailCommentDTO.setProductDetailId(this.getProductDetailId());
        productDetailCommentDTO.setComment(this.getComment());
        productDetailCommentDTO.setRate(this.getRate());
        productDetailCommentDTO.setUserId(this.getUserId());
        return productDetailCommentDTO;
    }
}
