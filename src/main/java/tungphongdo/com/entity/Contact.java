package tungphongdo.com.entity;

import tungphongdo.com.dto.ContactDTO;

import javax.persistence.*;

@Entity
@Table(name = "CONTACT")
public class Contact {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "PHONE")
    private Integer phone;

    @Column(name = "COMMENT")
    private String comment;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPhone() {
        return phone;
    }

    public void setPhone(Integer phone) {
        this.phone = phone;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public ContactDTO toModel(){
        ContactDTO contactDTO = new ContactDTO();
        contactDTO.setId(this.getId());
        contactDTO.setName(this.getName());
        contactDTO.setPhone(this.phone);
        contactDTO.setComment(this.comment);
        return contactDTO;
    }
}
