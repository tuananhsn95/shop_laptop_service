package tungphongdo.com.entity;

import tungphongdo.com.dto.ColorDTO;

import javax.persistence.*;

@Entity
@Table(name = "COLOR")
public class Color {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "COLOR_NAME")
    private String colorName;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public ColorDTO toModel(){
        ColorDTO colorDTO = new ColorDTO();
        colorDTO.setId(this.getId());
        colorDTO.setColorName(this.getColorName());
        return colorDTO;
    }
}
