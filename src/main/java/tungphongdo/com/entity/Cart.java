package tungphongdo.com.entity;

import tungphongdo.com.dto.CartDTO;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "CART")
public class Cart {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "PRODUCT_DETAIL_ID")
    private Integer productDetailId;

    @Column(name = "AMOUNT")
    private Integer amount;

    @Column(name = "DATETIME_EXPIRED")
    private Timestamp datetimeExpired;

    @Column(name = "USER_ID")
    private Integer userId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getProductDetailId() {
        return productDetailId;
    }

    public void setProductDetailId(Integer productDetailId) {
        this.productDetailId = productDetailId;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Timestamp getDatetimeExpired() {
        return datetimeExpired;
    }

    public void setDatetimeExpired(Timestamp datetimeExpired) {
        this.datetimeExpired = datetimeExpired;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public CartDTO toModel(){
        CartDTO cartDTO  = new CartDTO();
        cartDTO.setId(this.getId());
        cartDTO.setAmount(this.getAmount());
        cartDTO.setDatetimeExpired(this.getDatetimeExpired());
        cartDTO.setProductDetailId(this.getProductDetailId());
        cartDTO.setUserId(this.getUserId());
        return cartDTO;
    }
}
