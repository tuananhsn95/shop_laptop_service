package tungphongdo.com.entity;

import tungphongdo.com.dto.RoleDTO;

import javax.persistence.*;

@Entity
@Table(name = "USER")
public class Role {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "ROLE_NAME")
    private String roleName;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public RoleDTO toModel(){
        RoleDTO roleDTO = new RoleDTO();
        roleDTO.setId(this.getId());
        roleDTO.setRoleName(this.getRoleName());
        return roleDTO;
    }
}
