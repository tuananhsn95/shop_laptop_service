package tungphongdo.com.entity;

import tungphongdo.com.dto.BillDTO;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "BILL")
public class Bill {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "USER_ID")
    private Integer userId;

    @Column(name = "TOTAL_MONEY")
    private Double totalMoney;

    @Column(name = "DATETIME_BOUGHT")
    private Timestamp datetimeBought;

    @Column(name = "PAYMENT")
    private Integer payment;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Double getTotalMoney() {
        return totalMoney;
    }

    public void setTotalMoney(Double totalMoney) {
        this.totalMoney = totalMoney;
    }

    public Timestamp getDatetimeBought() {
        return datetimeBought;
    }

    public void setDatetimeBought(Timestamp datetimeBought) {
        this.datetimeBought = datetimeBought;
    }

    public Integer getPayment() {
        return payment;
    }

    public void setPayment(Integer payment) {
        this.payment = payment;
    }

    public BillDTO toModel(){
        BillDTO billDTO = new BillDTO();
        billDTO.setId(this.getId());
        billDTO.setUserId(this.getUserId());
        billDTO.setTotalMoney(this.getTotalMoney());
        billDTO.setDatetimeBought(this.getDatetimeBought());
        billDTO.setPayment(this.getPayment());
        return billDTO;
    }
}
