package tungphongdo.com.dto;

import tungphongdo.com.entity.Cart;

import java.sql.Timestamp;

public class CartDTO {
    private Integer id;
    private Integer productDetailId;
    private Integer amount;
    private Timestamp datetimeExpired;
    private Integer userId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getProductDetailId() {
        return productDetailId;
    }

    public void setProductDetailId(Integer productDetailId) {
        this.productDetailId = productDetailId;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Timestamp getDatetimeExpired() {
        return datetimeExpired;
    }

    public void setDatetimeExpired(Timestamp datetimeExpired) {
        this.datetimeExpired = datetimeExpired;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Cart toModel(){
        Cart cart  = new Cart();
        cart.setId(this.getId());
        cart.setAmount(this.getAmount());
        cart.setDatetimeExpired(this.getDatetimeExpired());
        cart.setProductDetailId(this.getProductDetailId());
        cart.setUserId(this.getUserId());
        return cart;
    }
}
