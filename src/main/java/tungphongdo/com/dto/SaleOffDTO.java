package tungphongdo.com.dto;

import tungphongdo.com.entity.SaleOff;

import java.sql.Timestamp;

public class SaleOffDTO {
    private Integer id;
    private String saleOffName;
    private Timestamp startTime;
    private Timestamp endTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSaleOffName() {
        return saleOffName;
    }

    public void setSaleOffName(String saleOffName) {
        this.saleOffName = saleOffName;
    }

    public Timestamp getStartTime() {
        return startTime;
    }

    public void setStartTime(Timestamp startTime) {
        this.startTime = startTime;
    }

    public Timestamp getEndTime() {
        return endTime;
    }

    public void setEndTime(Timestamp endTime) {
        this.endTime = endTime;
    }

    public SaleOff toModel(){
        SaleOff saleOff = new SaleOff();
        saleOff.setId(this.id);
        saleOff.setSaleOffName(this.getSaleOffName());
        saleOff.setStartTime(this.startTime);
        saleOff.setEndTime(this.getEndTime());
        return saleOff;
    }
}
