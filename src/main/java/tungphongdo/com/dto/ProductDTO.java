package tungphongdo.com.dto;

import tungphongdo.com.entity.Product;

public class ProductDTO {

    private Integer id;
    private Integer categoryId;
    private String productName;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Product toModel(){
        Product product = new Product();
        product.setId(this.getId());
        product.setProductName(this.getProductName());
        product.setCategoryId(this.getCategoryId());
        return product;
    }
}
