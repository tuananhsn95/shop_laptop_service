package tungphongdo.com.dto;

import tungphongdo.com.entity.Transportation;

public class TransportationDTO {

    private Integer id;
    private Integer billId;
    private Integer statusBill;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getBillId() {
        return billId;
    }

    public void setBillId(Integer billId) {
        this.billId = billId;
    }

    public Integer getStatusBill() {
        return statusBill;
    }

    public void setStatusBill(Integer statusBill) {
        this.statusBill = statusBill;
    }

    public Transportation toModel(){
        Transportation transportation = new Transportation();
        transportation.setId(this.getId());
        transportation.setBillId(this.getBillId());
        transportation.setStatusBill(this.getStatusBill());
        return transportation;
    }
}
