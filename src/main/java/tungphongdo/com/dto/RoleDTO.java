package tungphongdo.com.dto;

import tungphongdo.com.entity.Role;

public class RoleDTO {
    private Integer id;
    private String roleName;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public Role toModel(){
        Role role = new Role();
        role.setId(this.getId());
        role.setRoleName(this.getRoleName());
        return role;
    }


}
