package tungphongdo.com.dto;

import tungphongdo.com.entity.Size;

public class SizeDTO {

    private Integer id;
    private Double screenSize;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getScreenSize() {
        return screenSize;
    }

    public void setScreenSize(Double screenSize) {
        this.screenSize = screenSize;
    }

    public Size toModel(){
        Size size = new Size();
        size.setId(this.getId());
        size.setScreenSize(this.getScreenSize());
        return size;
    }
}
