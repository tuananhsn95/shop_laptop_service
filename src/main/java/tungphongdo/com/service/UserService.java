package tungphongdo.com.service;

import tungphongdo.com.dto.UserDTO;
import tungphongdo.com.exception.ResourceNotFoundException;
import tungphongdo.com.resourceRequest.ChangePassRequest;
import tungphongdo.com.resourceRequest.LoginRequest;
import tungphongdo.com.resourceResponse.CommonResponse;
import tungphongdo.com.resourceResponse.GetDetailResponse;
import tungphongdo.com.resourceResponse.LoginResponse;

public interface UserService {
    LoginResponse login(LoginRequest loginRequest) throws ResourceNotFoundException;

    CommonResponse createUser(UserDTO userDTO);

    CommonResponse forgotPass(String email) throws ResourceNotFoundException;

    GetDetailResponse userDetail(Integer userId) throws Exception;

    CommonResponse changePass(ChangePassRequest changePassRequest) throws Exception;
}
