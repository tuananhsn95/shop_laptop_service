package tungphongdo.com.service.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tungphongdo.com.commons.DataUtils;
import tungphongdo.com.dto.ProductDTO;
import tungphongdo.com.dto.ProductDetailDTO;
import tungphongdo.com.repository.ProductDetailRepository;
import tungphongdo.com.repository.ProductRepository;
import tungphongdo.com.resourceRequest.GetAllProductRequest;
import tungphongdo.com.resourceRequest.SearchRequest;
import tungphongdo.com.resourceResponse.ServiceResult;
import tungphongdo.com.service.ProductService;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class ProductDetailServiceImpl implements ProductService {

//    private final Logger log = LoggerFactory.getLogger(ProductDetailServiceImpl.class);

    @Autowired
    private ResourceBundleMessageSource messageSource;

    @Autowired
    private ProductDetailRepository productDetailRepository;

    @Autowired
    private ProductRepository productRepository;

    @Override
    @Transactional
    public ServiceResult<ProductDetailDTO> getAllProduct(SearchRequest<GetAllProductRequest> productSearchRequest) {
        Pageable pageable = PageRequest.of(productSearchRequest.getPage(), productSearchRequest.getPageSize());

        Page<Object[]> listProducts = productDetailRepository.getAllProduct(productSearchRequest.getData().getCategoryId(),
                productSearchRequest.getData().getProductId(),
                productSearchRequest.getData().getScreenSize(),
                productSearchRequest.getData().getColorName(),
                pageable);

        List<ProductDetailDTO> listProductsResponse = new ArrayList<>();
        if (!listProducts.isEmpty()) {

            listProducts.getContent().stream().forEach(product -> {
                ProductDetailDTO productDetailDTO = new ProductDetailDTO();
                productDetailDTO.setId(DataUtils.safeToInt(product[0]));
                productDetailDTO.setProductId(DataUtils.safeToInt(product[1]));
                productDetailDTO.setProductName(DataUtils.safeToString(product[2]));
                productDetailDTO.setCategoryId(DataUtils.safeToInt(product[3]));
                productDetailDTO.setCategoryName(DataUtils.safeToString(product[4]));
                productDetailDTO.setSizeId(DataUtils.safeToInt(product[5]));
                productDetailDTO.setScreenSize(DataUtils.safeToDouble(product[6]));
                productDetailDTO.setColorId(DataUtils.safeToInt(product[7]));
                productDetailDTO.setColorName(DataUtils.safeToString(product[8]));
                productDetailDTO.setAmount(Integer.parseInt(product[9].toString()));
                productDetailDTO.setSinglePrice(DataUtils.safeToDouble(product[10]));

                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                productDetailDTO.setDatetimeImported(LocalDateTime.parse(product[11].toString(), formatter));

                productDetailDTO.setDescription(DataUtils.safeToString(product[12]));
                productDetailDTO.setImage(DataUtils.safeToString(product[13]));
                listProductsResponse.add(productDetailDTO);
            });

        }

        return new ServiceResult<>(
                listProductsResponse,
                listProducts.getTotalElements(),
                listProducts.getTotalPages(),
                listProducts.getPageable().getPageNumber(),
                listProducts.getPageable().getPageSize(),
                messageSource.getMessage("change.pass.successfully", null, LocaleContextHolder.getLocale()),
                HttpStatus.OK.value());
    }

    @Override
    public ProductDTO getProductById(Integer productId) {
        ProductDTO productDTO = productRepository.getProductById(productId).toModel();
        return productDTO;
    }
}
